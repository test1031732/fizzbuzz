# Backend - Spring Boot Fizz Buzz

## Description

Écrivez une petite application Spring Boot avec une API REST, qui reçoit une valeur en entrée
et qui renvoie une liste de résultats au format JSON.

En entrant un nombre, l'application renvoie une liste de valeurs à partir de 1 jusqu'au nombre
saisi en tenant compte des règles suivantes :

- Si le nombre est divisible par 3, vous devez retourner Fizz
- Si le nombre est divisible par 5, vous devez retourner Buzz
- Si le nombre est divisible par 3 et 5 à la fois, vous devez retourner FizzBuzz
- Sinon, il vous suffit de sortir le numéro suivant

## TDD

On commence par l'analyse de problème. Pour créer une API REST qui retourne un JSON,
on aura besoin d'un Controller pour recevoir l'appel GET et d'un Service qui sera appelé par
le contrôleur pour traiter la valeur en entrée et retourner le résultat.

Alors en TDD, on va commencer par l'écriture des cas de tests, qui lors d'un premier lancement
vont échouer, ensuite on écrit le code qui va avec et on vérifie que les tests sont OK.

Enfin on fait un revu du code, pour modifier et faire un refactoring la ou il faut le faire,
et bien sur un test final.

### FizzBuzzServiceTest

Dans une première étape on va commencer par la création de la classe de test FizzBuzzServiceTest.java.
Dans cette classe on va ajouter nos cas de tests, et après chaque cas de tests on reteste pour vérifier
que tous les cas précédent passe bien sinon il faut rectifier le code

| Test           | Entrée | Résultat Attendu (Liste)                                                                    |
|----------------|--------|---------------------------------------------------------------------------------------------|
| testEvaluate1  | 1      | 1                                                                                           |
| testEvaluate10 | 10     | Buzz                                                                                        |
| testEvaluate3  | 3      | Fizz                                                                                        |
| testEvaluate5  | 5      | Buzz                                                                                        |
| testEvaluate15 | 15     | FizzBuzz                                                                                    |
| testPlay1      | 1      | 1                                                                                           |
| testPlay3      | 3      | 12Fizz                                                                                      |
| testPlay5      | 5      | 12Fizz4Buzz                                                                                 |
| testPlay10     | 10     | 12Fizz4BuzzFizz78FizzBuzz                                                                   |
| testPlay15     | 16     | 12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz                                                 |
| testPlay30     | 32     | 12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz1617Fizz19BuzzFizz2223FizzBuzz26Fizz2829FizzBuzz |

### FizzBuzzControllerTest

Ensuite, il faut créer une classe pour les tests du contrôleur, dans ce cas on aura besoin d'utiliser
WebMvcTest et MockBean.

## Extra

- Un exception handler ApiExceptionHandler pour intercepter MethodArgumentTypeMismatchException