package com.manageo.fizzbuzz;

import com.manageo.fizzbuzz.controller.FizzBuzzController;
import com.manageo.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FizzBuzzController.class)
class FizzBuzzControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FizzBuzzService fizzBuzzService;

    @Test
    public void testFizzBuzzEndpoint() throws Exception {
        when(fizzBuzzService.play(anyInt())).thenReturn(List.of());
        mockMvc.perform(get("/v1/fizzbuzz/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testFizzBuzzPlay5() throws Exception {
        when(fizzBuzzService.play(anyInt())).thenReturn(List.of("1", "2", "Fizz", "4", "Buzz"));

        mockMvc.perform(get("/v1/fizzbuzz/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\"]"));
    }

    @Test
    public void testFizzBuzzPlayException() throws Exception {
        mockMvc.perform(get("/v1/fizzbuzz/aaa")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}