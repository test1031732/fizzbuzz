package com.manageo.fizzbuzz;

import com.manageo.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class FizzBuzzServiceTest {
    @Autowired
    private FizzBuzzService fizzBuzzService;

    @Test
    public void testAutowire() {
        assertNotNull(fizzBuzzService);
    }

    @Test
    public void testEvaluate1() {
        assertEquals("1", fizzBuzzService.evaluate(1));
    }

    @Test
    public void testEvaluate10() {
        assertEquals("Buzz", fizzBuzzService.evaluate(10));
    }

    @Test
    public void testEvaluate3() {
        assertEquals("Fizz", fizzBuzzService.evaluate(3));
    }

    @Test
    public void testEvaluate5() {
        assertEquals("Buzz", fizzBuzzService.evaluate(5));

    }

    @Test
    public void testEvaluate15() {
        assertEquals("FizzBuzz", fizzBuzzService.evaluate(15));
    }

    @Test
    public void testPlay1() {
        assertEquals(Collections.singletonList("1"), fizzBuzzService.play(1));
    }

    @Test
    public void testPlay3() {
        assertEquals(List.of("1", "2", "Fizz"), fizzBuzzService.play(3));
    }

    @Test
    public void testPlay5() {
        assertEquals(List.of("1", "2", "Fizz", "4", "Buzz"), fizzBuzzService.play(5));
    }

    @Test
    public void testPlay10() {
        assertEquals(List.of("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"),
                fizzBuzzService.play(10));
    }

    @Test
    public void testPlay15() {
        assertEquals(List.of("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz",
                "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"), fizzBuzzService.play(15));
    }

    @Test
    public void testPlay32() {
        assertEquals(List.of("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz",
                "11", "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23",
                "Fizz", "Buzz", "26", "Fizz", "28", "29", "FizzBuzz", "31", "32"), fizzBuzzService.play(32));
    }
}