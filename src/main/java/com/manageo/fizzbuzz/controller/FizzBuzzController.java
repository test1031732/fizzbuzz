package com.manageo.fizzbuzz.controller;

import com.manageo.fizzbuzz.service.FizzBuzzService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/fizzbuzz")
public class FizzBuzzController {
    private final FizzBuzzService fizzBuzzService;

    public FizzBuzzController(FizzBuzzService fizzBuzzService) {
        this.fizzBuzzService = fizzBuzzService;
    }

    @GetMapping(value = "/{number}", produces = "application/json")
    public ResponseEntity<List<String>> playFizzBuz(@PathVariable int number) {
        return new ResponseEntity<>(this.fizzBuzzService.play(number), HttpStatus.OK);
    }
}