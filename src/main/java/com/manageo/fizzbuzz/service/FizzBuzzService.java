package com.manageo.fizzbuzz.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FizzBuzzService {

    public String evaluate(int i) {
        if (i % 3 == 0 && i % 5 == 0) {
            return "FizzBuzz";
        } else if (i % 5 == 0) {
            return "Buzz";
        } else if (i % 3 == 0) {
            return "Fizz";
        } else {
            return String.valueOf(i);
        }
    }

    public List<String> play(int number) {
        List<String> results = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            results.add(this.evaluate(i));
        }
        return results;
    }
}